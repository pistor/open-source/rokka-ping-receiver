<?php

namespace Drupal\rokka_ping_receiver\Controller;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\file\Entity\File;
use Drupal\media\Entity\Media;
use Drupal\rokka\Entity\RokkaMetadata;
use Drupal\rokka\RokkaServiceInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class PingController.
 */
class PingController extends ControllerBase {

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Drupal\Core\Entity\EntityManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityManager;

  /**
   * File storage service.
   *
   * @var \Drupal\Core\Entity\ContentEntityStorageInterface
   */
  protected $fileStorage;

  /**
   * Logger initialised to this module’s channel.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Media entity storage manager..
   *
   * @var \Drupal\Core\Entity\ContentEntityStorageInterface
   */
  protected $mediaStorage;

  /**
   * Rokka Metadata storage service.
   *
   * @var \Drupal\Core\Entity\ContentEntityStorageInterface
   */
  protected $rokkaMetadataStorage;

  /**
   * Drupal\rokka\RokkaServiceInterface definition.
   *
   * @var \Drupal\rokka\RokkaServiceInterface
   */
  protected $rokkaService;

  /**
   * Constructs a new PingController object.
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_manager, LoggerInterface $logger, RokkaServiceInterface $rokka_service) {
    $this->configFactory = $config_factory;
    $this->entityManager = $entity_manager;
    $this->fileStorage = $entity_manager->getStorage('file');
    $this->logger = $logger;
    $this->mediaStorage = $entity_manager->getStorage('media');
    $this->rokkaMetadataStorage = $entity_manager->getStorage('rokka_metadata');
    $this->rokkaService = $rokka_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager'),
      $container->get('logger.channel.rokka_ping_receiver'),
      $container->get('rokka.service')
    );
  }

  /**
   * Clean file name to use as alt-text.
   *
   * @param $file_name
   * @return string
   */
  public static function cleanFileName($file_name) {
    $clean_name = preg_replace('/[_-]+/', ' ', $file_name);
    $clean_name = preg_replace('/\.[\w ]+$/', '', $clean_name);
    return trim($clean_name);
  }

  /**
   * Recieve ping via POST request.
   *
   * @return string
   *   Return Hello string.
   */
  public function post(Request $request) {
    $response = [];

    $rokkaConf = $this->configFactory->get('rokka.settings');

    $origin_digest =  $request->headers->get('Auth-Digest');
    $payload = $request->getContent();
    $our_digest = hash_hmac('sha256', $payload, $rokkaConf->get('api_key'));

    if (!hash_equals($origin_digest, $our_digest)) {
      return Response::create('Invalid authentication', Response::HTTP_UNAUTHORIZED);
    }

    if (strpos($request->headers->get('Content-Type'),'application/json' ) === 0) {
      $data = json_decode($payload, TRUE);

      $response['status']['ping'] = 'RECEIVED';

      $this->logger->notice('%type ping received for image @hash.', [
        '%type' => $data['pingType'] ?? 'unknown',
        '@hash' => $data['binary_hash'] ?? '?'
      ]);

      $metadata = $this->rokkaMetadataStorage->loadByProperties(['binary_hash' => $data['binary_hash']]);

      // Create metadata entity if it does not exist.
      if (empty($metadata)) {
        $uri = 'rokka://' . $data['organization'] . '/' . $data['binary_hash'] . '.' . $data['format'];
        $new_metadata = RokkaMetadata::create([
          // 0 for anonymous user.
          'user_id' => 0,
          'hash' => $data['hash'],
          'binary_hash' => $data['binary_hash'],
          'uri' => $uri,
          'filesize' => $data['size'],
          'height' => $data['height'],
          'width' => $data['width'],
          'format' => $data['format'],
          'status' => 1,
          'created' => strtotime($data['created']),
        ]);
        $new_metadata->save();

        $this->logger->info('New Rokka metadata entity #@id created for binary hash @hash.', [
          '@id' => $new_metadata->id(),
          '@hash' => $data['binary_hash'] ?? '?'
        ]);
        $response['status']['metadata'] = 'CREATE';


        $metadata[] = $new_metadata;
      }

      $files = $this->fileStorage->loadByProperties(['uri' => array_map(function($item) {
        return $item->getUri();
      }, $metadata)]);

      // Create file entity if it does not exist.
      if (empty($files)) {
        // Use the first found metadata as parent.
        $parent = reset($metadata);

        $file = File::create([
          'uid' => $parent->getOwnerId(),
          'filename' => $data['fileName'],
          'uri' => $parent->getUri(),
          'filemime' => \GuzzleHttp\Psr7\MimeType::fromExtension($parent->getFormat()),
          'filesize' => $parent->getFilesize(),
          'status' => 1,
          'created' => $parent->getCreatedTime(),
        ]);
        $file->save();

        $this->logger->info('New file entity #@id created for uri @uri.', [
          '@id' => $file->id(),
          '@uri' => $parent->getUri(),
        ]);
        $response['status']['file'] = 'CREATE';

        $files[] = $file;
      }

      $media_ids = $this->mediaStorage->getQuery()
        ->condition('field_media_image.target_id', array_map(function ($item) {
          return $item->id();
        }, $files), 'IN')
        ->execute();

      $clean_name = self::cleanFileName($data['fileName']);

      foreach ($media_ids as $media_id) {
        $media = Media::load($media_id);

        $media->setName($clean_name);
        $media->field_media_image->get(0)->set('alt', $clean_name);
        $media->save();
      }

      // Create media entity if need be.
      if (empty($media_ids)) {
        $parent = reset($files);

        $media = Media::create([
          'bundle' => 'image',
          'name' => $clean_name,
          'thumbnail' => [
            'target_id' => $parent->id(),
            'title' => $clean_name,
            'alt' => 'Thumbnail (' . $clean_name . ')',
            'height' => $data['height'],
            'width' => $data['width'],
          ],
          'field_media_image' => [
            'target_id' => $parent->id(),
            'alt' => $clean_name,
            'height' => $data['height'],
            'width' => $data['width'],
          ]
        ]);

        $media->save();

        $this->logger->info('New media entity #@id created for image @parent_id.', [
          '@id' => $media->id(),
          '@parent_id' => $parent->id(),
        ]);
        $response['status']['media'] = 'CREATE';
      }
    }

    return new JsonResponse($response);
  }
}
