Rokka ping receiver
===================

Receive pings from [rokka-uploader][] and create corresponding file- and media entities in [Drupal][].

Uses the [Rokka] module.

[rokka-uploader]: https://gitlab.com/pistor/open-source/rokka-uploader
[Drupal]: https://www.drupal.org/
[Rokka]: https://rokka.io/
